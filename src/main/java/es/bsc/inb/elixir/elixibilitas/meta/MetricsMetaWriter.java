package es.bsc.inb.elixir.elixibilitas.meta;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.json.stream.JsonGenerator;

/**
 * @author Dmitry Repchevsky
 */
        
public class MetricsMetaWriter extends AbstractMeta implements AutoCloseable {
    
    private final JsonGenerator generator;
    
    private String label;
    final Map<String, JsonValue> map = new LinkedHashMap<>();

    public MetricsMetaWriter(final JsonGenerator generator) {
        this.generator = generator;
    }
    
    public void write(final JsonValue value) {
        if (JsonValue.ValueType.OBJECT == value.getValueType()) {
            final JsonObject o = value.asJsonObject();

            String id = o.getJsonString("@id").getString();
            id = id.substring(id.indexOf("metrics") + 8); // "metrics/".length()
            final String label = getLabel(id);
            if (this.label == null) {
                this.label = label;
            } else if (!this.label.equals(label)) {
                this.label = label;
                final JsonValue meta = map.get("/");
                map.clear();
                travers(id, "/", null, o, Json.createObjectBuilder().build(), map);
                generator.write(meta);
            } 

            travers(id, "/", null, o, Json.createObjectBuilder().build(), map);
        }

    }

    private String getLabel(String id) {
        final String[] nodes = id.split("/");
        if (nodes.length > 0) {
            final String[] _id = nodes[0].split(":");
            return _id[_id.length > 1 ? 1 : 0];
        }
        
        return "";
    }
    @Override
    public void close() throws IOException {
        final JsonValue meta = map.get("/");
        generator.write(meta);
        generator.close();
    }
}
