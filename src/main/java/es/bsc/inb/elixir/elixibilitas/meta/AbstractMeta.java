/**
 * *****************************************************************************
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.elixibilitas.meta;

import java.util.Map;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonValue;

/**
 * @author Dmitry Repchevsky
 */
        
public abstract class AbstractMeta {
    
    /**
     * Traverses the JSON value and updates the parent element (object or array).
     * 
     * @param id tool's identifier
     * @param path JSON path to the value
     * @param name value's name (null if parent is an array)
     * @param value currently traversed JSON value
     * @param parent parent of traversed value
     * @param map the map to keep JSON paths and values
     * 
     * @return an updated parent element
     */
    JsonValue travers(final String id,
                      final String path,
                      final String name,
                      final JsonValue value,
                      final JsonValue parent,
                      final Map<String, JsonValue> map) {
        
        final JsonValue.ValueType type = value.getValueType();
        if (type == JsonValue.ValueType.OBJECT) {
            if (parent.getValueType() == JsonValue.ValueType.OBJECT) {
                JsonValue object = map.get(path);
                if (object == null) {
                    object = Json.createObjectBuilder().build();
                }
                for (Map.Entry<String, JsonValue> entry : value.asJsonObject().entrySet()) {
                    object = travers(id, path + entry.getKey() + "/", entry.getKey(), entry.getValue(), object, map);
                }
                map.put(path, object);
                return name == null ? Json.createObjectBuilder(object.asJsonObject()).build() :
                                      Json.createObjectBuilder(parent.asJsonObject()).add(name, object).build();
            }
            
            final JsonArrayBuilder ab = Json.createArrayBuilder(parent.asJsonArray());
            
            JsonValue object = Json.createObjectBuilder().build();
            for (Map.Entry<String, JsonValue> entry : value.asJsonObject().entrySet()) {
                object = travers(id, path + entry.getKey() + "/", entry.getKey(), entry.getValue(), object, map);
            }
            ab.add(object);
            return ab.build();
        }
        
        JsonValue array = map.get(path);

        if (type == JsonValue.ValueType.ARRAY) {
            if (array == null) {
                array = Json.createArrayBuilder().build();
            } else {
                array = Json.createArrayBuilder(array.asJsonArray()).build();
            }

            int i = 0;
            for (JsonValue v : value.asJsonArray()) {
                array = travers(id, path + i++ + "/", null, v, array, map);
            }
        } else if (name != null && name.startsWith("@")) {
            return parent;
        } else if (array == null) {
            array = Json.createArrayBuilder().add(id + path).build();
        } else {
            array = Json.createArrayBuilder().add(id + path).build();
        }

        map.put(path, array);

        if (parent != null) {
            switch(parent.getValueType()) {
                case OBJECT: return Json.createObjectBuilder(parent.asJsonObject()).add(name, array).build();
                case ARRAY:  return Json.createArrayBuilder(parent.asJsonArray()).add(array).build();
            }
        }

        return parent;
    }

}
