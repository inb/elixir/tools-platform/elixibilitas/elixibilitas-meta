/**
 * *****************************************************************************
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.elixibilitas.meta;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Stream;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonValue;

/**
 * @author Dmitry Repchevsky
 */
        
public class ToolsMetaIterator extends AbstractMeta implements Iterator<JsonValue> {
    
    private final Iterator<JsonValue> iterator;
    private String label;
    private final Map<String, JsonValue> map = new LinkedHashMap<>();

    public ToolsMetaIterator(final Stream<JsonValue> stream) {
        iterator = stream.iterator();
    }
    
    @Override
    public boolean hasNext() {
        return iterator.hasNext() || !map.isEmpty();
    }

    @Override
    public JsonValue next() {
        while (iterator.hasNext()) {
            JsonValue value = iterator.next();
            if (JsonValue.ValueType.OBJECT == value.getValueType()) {
                final JsonObject o = value.asJsonObject();

                String id = o.getJsonString("@id").getString();
                id = id.substring(id.indexOf("tool") + 5); // "tool/".length()
                final String label = o.getJsonString("@label").getString();
                if (this.label == null) {
                    this.label = label;
                } else if (!this.label.equals(label)) {
                    this.label = label;
                    final JsonValue meta = map.get("/");
                    map.clear();
                    travers(id, "/", null, o, Json.createObjectBuilder().build(), map);
                    return meta;
                } 

                travers(id, "/", null, o, Json.createObjectBuilder().build(), map);
            }
        }
        final JsonValue meta = map.get("/");
        map.clear();
        return meta;
    }
}
